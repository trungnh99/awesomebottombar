import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import en from './i18n/en_US.json';
import vi from './i18n/vi_VN.json';
//-------------------------------------

export const languagesLabel = [
  {
    code: 'en-US',
    text: 'English',
  },
  {
    code: 'vi-VN',
    text: '日本語',
  },
];

i18n.use(initReactI18next).init({
  interpolation: {escapeValue: false},
  fallbackLng: 'vi',
  lng: 'en-US',
  resources: {
    en: {
      translation: en,
    },
    vi: {
      translation: vi,
    },
  },
});

export default i18n;
