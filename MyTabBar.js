/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import * as shape from 'd3-shape';
import 'd3-path';
import 'd3';

import Icon from 'react-native-vector-icons/FontAwesome5';

const {width} = Dimensions.get('window');
const height = 64;

const tabs = [
  {
    name: 'grid',
  },
  {
    name: 'list',
  },
  {
    name: 'repeat',
  },
  {
    name: 'map',
  },
  {
    name: 'user',
  },
];

const tabWidth = width / tabs.length;
const backgroundColor = 'white';

const getPath = () => {
  const left = shape
    .line()
    .x((d) => d.x)
    .y((d) => d.y)([
    {x: 0, y: 0},
    {x: width / 2 - tabWidth / 2, y: 0},
  ]);
  const tab = shape
    .line()
    .x((d) => d.x)
    .y((d) => d.y)
    .curve(shape.curveBasis)([
    {x: width / 2 - tabWidth / 2, y: 0},
    {x: width / 2 - tabWidth / 2 + 2, y: 1},
    {x: width / 2 - tabWidth / 2 + 5, y: 2},
    {x: width / 2 - tabWidth / 2 + 10, y: 10},
    {x: width / 2 - tabWidth / 2 + 15, y: height / 4},
    {x: width / 2 - tabWidth / 2 + 20, y: height / 4 + 5},
    {x: width / 2 - tabWidth / 2 + 30, y: height / 4 + 15},
    // {x: width / 2 - tabWidth / 2 + 35, y: height / 4 + 20},
    // {x: width / 2 - tabWidth / 2 - 35, y: height / 4 + 20},
    {x: width / 2 + tabWidth / 2 - 30, y: height / 4 + 15},
    {x: width / 2 + tabWidth / 2 - 20, y: height / 4 + 5},
    {x: width / 2 + tabWidth / 2 - 15, y: height / 4},
    {x: width / 2 + tabWidth / 2 - 10, y: 10},
    {x: width / 2 + tabWidth / 2 - 5, y: 2},
    {x: width / 2 + tabWidth / 2 - 2, y: 1},
    {x: width / 2 + tabWidth / 2, y: 0},
  ]);
  const right = shape
    .line()
    .x((d) => d.x)
    .y((d) => d.y)([
    {x: width / 2 + tabWidth / 2, y: 0},
    {x: width, y: 0},
    {x: width, y: height},
    {x: 0, y: height},
    {x: 0, y: 0},
  ]);
  return `${left} ${tab} ${right}`;
};
const d = getPath();

export default function MyTabBar({state, descriptors, navigation}) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View
      style={{
        backgroundColor: 'rgba(52, 52, 52, 0)',
        position: 'absolute',
        bottom: 0,
      }}>
      <View
        {...{height, width}}
        style={{
          shadowOffset: {
            width: 10,
            height: 10,
          },
          shadowOpacity: 0.5,
          shadowRadius: 5,
        }}>
        <Svg width={width} height={height}>
          <Path fill={backgroundColor} {...{d}} />
        </Svg>
        <View style={[StyleSheet.absoluteFill, {flexDirection: 'row'}]}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            if (route.name === 'Home') {
              return (
                <TouchableOpacity
                  style={{
                    width: tabWidth,
                    display: 'flex',
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                  }}
                  key={index}
                  accessibilityRole="button"
                  accessibilityState={isFocused ? {selected: true} : {}}
                  accessibilityLabel={options.tabBarAccessibilityLabel}
                  testID={options.tabBarTestID}
                  onPress={onPress}
                  onLongPress={onLongPress}>
                  <View
                    style={{
                      position: 'absolute',
                      top: -35,
                      width: 60,
                      height: 60,
                      backgroundColor: 'white',
                      borderRadius: 30,
                      display: 'flex',
                      justifyContent: 'center',
                      alignContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon name="rocket" size={35} color="#900" />
                  </View>

                  <Text
                    style={{
                      color: isFocused ? '#673ab7' : '#222',
                      position: 'absolute',
                      bottom: 10,
                    }}>
                    {label}
                  </Text>
                </TouchableOpacity>
              );
            }

            return (
              <TouchableOpacity
                style={{
                  width: tabWidth,
                  display: 'flex',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                }}
                key={index}
                accessibilityRole="button"
                accessibilityState={isFocused ? {selected: true} : {}}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}>
                <View
                  style={{
                    position: 'absolute',
                    top: -8,
                    width: 60,
                    height: 60,
                    // backgroundColor: 'white',
                    // borderRadius: 30,
                    display: 'flex',
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon name="rocket" size={30} color="#900" />
                </View>

                <Text
                  style={{
                    color: isFocused ? '#673ab7' : '#222',
                    position: 'absolute',
                    bottom: 10,
                  }}>
                  {label}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
      {/* <SafeAreaView /> */}
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor,
//   },
// });
