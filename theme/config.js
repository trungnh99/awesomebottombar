// import {DefaultTheme} from 'react-native-paper';
import {StyleSheet, Dimensions} from 'react-native';
import NativeTachyons from 'react-native-style-tachyons';
import css from './presetCss';

const {width} = Dimensions.get('window');

const theme = {
  // ...DefaultTheme,
  roundness: 2,
  colors: {
    // ...DefaultTheme.colors,
    primary: '#F17B6B',
    backgroundCustom: '#F68E81',
    error: '#F0A69B',
    errorFocus: '#ba000d',
    text: '#ffffff',
    button: '#E96F5F',
    require: '#f44336',
    active: '#d32f2f',
    inputLabel: '#000000',
  },
};

export const tachyonsConfig = () =>
  NativeTachyons.build(
    {
      /* REM parameter is optional, default is 16 */
      rem: width > 340 ? 18 : 15,
      /* fontRem parameter is optional to allow adjustment in font-scaling. default falls back to rem */
      fontRem: 20,
      colors: {
        palette: theme.colors,
      },
      customStyles: {
        container: css.container,
        centerContent: css.centerContent,
        startEl: css.startEl,
        sbEl: css.sbEl,
      },
    },
    StyleSheet,
  );

export default theme;
