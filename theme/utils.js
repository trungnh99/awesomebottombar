import NativeTachyons, {styles} from 'react-native-style-tachyons';
import presetCss from './presetCss';

export const withCls = (component) => NativeTachyons.wrap(component);

export const tachyons = styles;

export const css = presetCss;
