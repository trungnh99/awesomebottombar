//@flow
import React, {useState} from 'react';
import {View, Text, Button, Image} from 'react-native';

import {useTranslation} from 'react-i18next';
import {withCls} from './theme/utils';

import crashlytics from '@react-native-firebase/crashlytics';

import codePush from 'react-native-code-push';

const imageView = [
  <Image source={require('./assets/images/com.jpg')} cls="h4 w4" />,
  <Image source={require('./assets/images/chao.jpg')} cls="h4 w4" />,
  <Image source={require('./assets/images/bun.jpg')} cls="h4 w4" />,
  <Image source={require('./assets/images/pho.jpg')} cls="h4 w4" />,
];

function Home() {
  const [idx, setIdx] = useState(0);

  const {t, i18n} = useTranslation();

  const randomIdx = async () => {
    for (let i = 0; i < 10; i++) {
      const newIdx = Math.floor(Math.random() * Math.floor(3));
      console.log('newIdx', newIdx);
      setIdx(newIdx);
      await new Promise((resolve) => setTimeout(() => resolve(), 200));
    }
  };
  return (
    <View cls="container">
      <Text cls="b primary">TEST CSS</Text>
      <Text cls="b primary">Test react native code push</Text>
      <Text cls="b primary">Code Push new version</Text>
      <View cls="h4 w4">{imageView[idx]}</View>

      <View>
        <Button title={t('random')} onPress={randomIdx} />
        <Button title="English" onPress={() => i18n.changeLanguage('en-US')} />
        <Button
          title="Vietnamese"
          onPress={() => i18n.changeLanguage('vi-VN')}
        />
        <Button title="Crash" onPress={() => crashlytics().crash()} />
        <Button
          title="Update"
          onPress={() =>
            codePush.sync({
              updateDialog: true,
              installMode: codePush.InstallMode.IMMEDIATE,
            })
          }
        />
      </View>
    </View>
  );
}

export default withCls(Home);
