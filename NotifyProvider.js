import React, {createContext, useEffect, useRef} from 'react';
import PushNotification from 'react-native-push-notification';
import NotificationHandler from './NotificationHandler';
import messaging from '@react-native-firebase/messaging';
//-----------------------------------------------------------------------------------------------
const NotificationContext = createContext();

const NotifyProvider = (props) => {
  const lastId = useRef(0);
  const lastChannelCounter = useRef(0);

  useEffect(() => {
    const unsubscribe = messaging().setBackgroundMessageHandler(
      async (remoteMessage) => {
        console.log('run handle background', remoteMessage);
      },
    );
    return unsubscribe;
  }, []);

  useEffect(() => {
    createDefaultChannels();

    //Get FCM Token
    messaging()
      .getToken()
      .then((token) => {
        console.log('token ', token);
      });

    NotificationHandler.attachNotification(onNotifications);

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(async (remoteMessage) => {
        console.log(
          'Notification caused app to open from quit state:',
          remoteMessage,
        );
      });

    // Clear badge number at start
    PushNotification.getApplicationIconBadgeNumber(function (number) {
      if (number > 0) {
        PushNotification.setApplicationIconBadgeNumber(0);
      }
    });

    PushNotification.getChannels(function (channels) {
      console.log(channels);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function onNotifications(notif) {
    if (notif.foreground && !notif.userInteraction) {
      //Foreground
      localNotify(notif.title, notif.message);
    }
  }

  function createDefaultChannels() {
    PushNotification.createChannel(
      {
        channelId: 'default-channel-id', // (required)
        channelName: 'Default channel', // (required)
        channelDescription: 'A default channel', // (optional) default: undefined.
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) =>
        console.log(`createChannel 'default-channel-id' returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
    );

    PushNotification.createChannel(
      {
        channelId: 'sound-channel-id', // (required)
        channelName: 'Sound channel', // (required)
        channelDescription: 'A sound channel', // (optional) default: undefined.
        soundName: 'sample.mp3', // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) =>
        console.log(`createChannel 'sound-channel-id' returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
    );
  }

  // function createOrUpdateChannel() {
  //   lastChannelCounter.current++;
  //   PushNotification.createChannel(
  //     {
  //       channelId: 'custom-channel-id', // (required)
  //       channelName: `Custom channel - Counter: ${lastChannelCounter.current}`, // (required)
  //       channelDescription: `A custom channel to categories your custom notifications. Updated at: ${Date.now()}`, // (optional) default: undefined.
  //       soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
  //       importance: 4, // (optional) default: 4. Int value of the Android notification importance
  //       vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
  //     },
  //     (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
  //   );
  // }

  // function popInitialNotification() {
  //   PushNotification.popInitialNotification((notification) =>
  //     console.log('InitialNotification:', notification),
  //   );
  // }

  function localNotify(title, message, soundName) {
    lastId.current++;
    PushNotification.localNotification({
      /* Android Only Properties */
      channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
      ticker: 'My Notification Ticker', // (optional)
      autoCancel: true, // (optional) default: true
      largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
      smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
      bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
      subText: 'This is a subText', // (optional) default: none
      color: 'red', // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      tag: 'some_tag', // (optional) add tag to message
      group: 'group', // (optional) add group to message
      groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
      ongoing: false, // (optional) set whether this is an "ongoing" notification
      // actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
      invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      when: null, // (optional) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
      usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
      timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

      /* iOS only properties */
      alertAction: 'view', // (optional) default: view
      category: '', // (optional) default: empty string

      /* iOS and Android properties */
      id: lastId.current, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      title: title, // (optional)
      message: message,
      userInfo: {screen: 'home'}, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: !!soundName, // (optional) default: true
      soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      //   number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
  }

  // function scheduleNotify(soundName) {
  //   lastId.current++;
  //   PushNotification.localNotificationSchedule({
  //     date: new Date(Date.now() + 30 * 1000), // in 30 secs

  //     /* Android Only Properties */
  //     channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
  //     ticker: 'My Notification Ticker', // (optional)
  //     autoCancel: true, // (optional) default: true
  //     largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
  //     smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
  //     bigText: 'My big text that will be shown when notification is expanded', // (optional) default: "message" prop
  //     subText: 'This is a subText', // (optional) default: none
  //     color: 'blue', // (optional) default: system default
  //     vibrate: true, // (optional) default: true
  //     vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
  //     tag: 'some_tag', // (optional) add tag to message
  //     group: 'group', // (optional) add group to message
  //     groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
  //     ongoing: false, // (optional) set whether this is an "ongoing" notification
  //     actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
  //     invokeApp: false, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

  //     when: null, // (optional) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
  //     usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
  //     timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

  //     /* iOS only properties */
  //     alertAction: 'view', // (optional) default: view
  //     category: '', // (optional) default: empty string

  //     /* iOS and Android properties */
  //     id: lastId.current, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
  //     title: 'Scheduled Notification', // (optional)
  //     message: 'My Notification Message', // (required)
  //     userInfo: {screen: 'home'}, // (optional) default: {} (using null throws a JSON value '<null>' error)
  //     playSound: !!soundName, // (optional) default: true
  //     soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
  //     number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
  //   });
  // }

  // function checkPermission(cbk) {
  //   return PushNotification.checkPermissions(cbk);
  // }

  // function cancelNotify() {
  //   PushNotification.cancelLocalNotifications({id: '' + lastId.current});
  // }

  // function cancelAll() {
  //   PushNotification.cancelAllLocalNotifications();
  // }

  // function abandonPermissions() {
  //   PushNotification.abandonPermissions();
  // }

  // function getScheduledLocalNotifications(callback) {
  //   PushNotification.getScheduledLocalNotifications(callback);
  // }

  return <NotificationContext.Provider value={{}} {...props} />;
};

export default NotifyProvider;
