/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Animated, {
  useSharedValue,
  withSpring,
  useAnimatedStyle,
  useAnimatedGestureHandler,
  runOnJS,
} from 'react-native-reanimated';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Sound from 'react-native-sound';
import {Alert, StyleSheet, View} from 'react-native';

Sound.setCategory('Playback');

const correctSound = new Sound('correct.mp3', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});

function correctSoundPlay() {
  correctSound.play();
}

function App() {
  const pan = {
    x: useSharedValue(0),
    y: useSharedValue(0),
  };

  const original = {
    x: useSharedValue(0),
    y: useSharedValue(0),
  };

  const target = {
    x: useSharedValue(0),
    y: useSharedValue(0),
  };

  const isPanning = useSharedValue(false);
  const isValid = useSharedValue(false);

  const compare = (_value, _target) => {
    'worklet';
    if (_value.x === _target.x && _value.y === _target.y) {
      return true;
    }

    if (
      Math.abs(_value.x - _target.x) < 50 &&
      Math.abs(_value.y - _target.y) < 50
    ) {
      return true;
    }

    return false;
  };

  function alert() {
    Alert.alert('Great move', '');
  }

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startX = pan.x.value;
      ctx.startY = pan.y.value;
      isPanning.value = true;
    },
    onActive: (event, ctx) => {
      pan.x.value = ctx.startX + event.translationX;
      pan.y.value = ctx.startY + event.translationY;

      if (
        compare(
          {
            x: pan.x.value + original.x.value,
            y: pan.y.value + original.y.value,
          },
          {x: target.x.value, y: target.y.value},
        )
      ) {
        isValid.value = true;
      } else {
        isValid.value = false;
      }
    },
    onEnd: (_, ctx) => {
      isPanning.value = false;
      console.log('pan', pan.x.value, pan.y.value);
      console.log(
        'x y ',
        pan.x.value + original.x.value,
        pan.y.value + original.y.value,
      );

      if (
        compare(
          {
            x: pan.x.value + original.x.value,
            y: pan.y.value + original.y.value,
          },
          {x: target.x.value, y: target.y.value},
        )
      ) {
        pan.x.value = target.x.value - original.x.value;
        pan.y.value = target.y.value - original.y.value;
        isValid.value = false;

        runOnJS(correctSoundPlay)();
        runOnJS(alert)();
      } else {
        runOnJS(correctSoundPlay)();
        pan.x.value = withSpring(0, {
          damping: 14,
        });
        pan.y.value = withSpring(0, {
          damping: 14,
        });
      }
    },
  });

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withSpring(pan.x.value),
          // translateX: pan.x.value,
        },
        {
          translateY: withSpring(pan.y.value),
          // translateY: pan.y.value,
        },
      ],
    };
  });

  const targetAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: withSpring(isValid.value ? 1.2 : 1),
        },
      ],
    };
  });

  return (
    <View
      style={{
        flex: 1,
      }}>
      <PanGestureHandler onGestureEvent={gestureHandler}>
        <Animated.View
          style={[
            {
              borderWidth: 1,
            },
            styles.container,
          ]}>
          <Animated.View
            style={[
              styles.box,
              {
                backgroundColor: '#ccc',
              },
              targetAnimatedStyle,
            ]}
            onLayout={({nativeEvent: {layout}}) => {
              console.log('target layout ', layout);
              target.x.value = layout.x;
              target.y.value = layout.y;
            }}
          />

          <Animated.View
            style={[
              styles.box,
              animatedStyle,
              {
                position: 'absolute',
                bottom: 70,
                right: 20,
              },
            ]}
            onLayout={({nativeEvent: {layout}}) => {
              console.log('layout ', layout);
              original.x.value = layout.x;
              original.y.value = layout.y;
            }}
          />
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
}

export default App;

const styles = StyleSheet.create({
  box: {
    height: 100,
    width: 100,
    backgroundColor: 'blue',
    borderRadius: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
