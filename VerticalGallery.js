import React, {useRef} from 'react';
import {Animated, StyleSheet, Image, View} from 'react-native';

const images = new Array(6).fill(
  'https://images.unsplash.com/photo-1556740749-887f6717d7e4',
);

const IMAGE_HEIGHT = 400;

const INDICATOR_SIZE = 6;

const VerticalGallery = () => {
  const scrollY = useRef(new Animated.Value(0)).current;

  return (
    <View style={styles.container}>
      <Animated.FlatList
        data={images}
        keyExtractor={(item, index) => index.toString()}
        snapToInterval={IMAGE_HEIGHT}
        decelerationRate="fast"
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <Image source={{uri: item}} style={styles.item} />
        )}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY,
                },
              },
            },
          ],
          {
            useNativeDriver: true,
          },
        )}
      />

      <View style={styles.pagination}>
        {images.map((_, idx) => (
          <View key={idx} style={styles.indicator} />
        ))}
        <Animated.View
          style={[
            {
              transform: [
                {
                  translateY: Animated.divide(
                    scrollY,
                    IMAGE_HEIGHT,
                  ).interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, INDICATOR_SIZE * 2],
                  }),
                },
              ],
            },
            styles.active,
          ]}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: IMAGE_HEIGHT,
    width: '100%',
  },
  item: {
    height: IMAGE_HEIGHT,
    width: '100%',
    resizeMode: 'cover',
  },
  indicator: {
    width: INDICATOR_SIZE,
    height: INDICATOR_SIZE,
    borderRadius: INDICATOR_SIZE,
    backgroundColor: '#fff',
    marginVertical: INDICATOR_SIZE / 2,
    marginHorizontal: INDICATOR_SIZE / 2,
  },
  pagination: {
    position: 'absolute',
    left: 20,
    top: IMAGE_HEIGHT / 2,
  },
  active: {
    width: INDICATOR_SIZE * 2,
    height: INDICATOR_SIZE * 2,
    borderRadius: INDICATOR_SIZE,
    position: 'absolute',
    // borderWidth: 1,
    backgroundColor: '#333',
  },
});

export default VerticalGallery;
