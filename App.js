/* eslint-disable react-native/no-inline-styles */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import 'react-native-gesture-handler';

import MyTabBar from './MyTabBar';
import codePush from 'react-native-code-push';

import {Text, View, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from './HomeComponent';
import Gallery from './Gallery';
import NotifyProvider from './NotifyProvider';
import Reanimate from './Reanimated';
import VerticalGallery from './VerticalGallery';
import ScrollEvent from './ScrollEvent';

import {RecoilRoot, atom, useRecoilState, useResetRecoilState} from 'recoil';
import './i18n';

const counter = atom({
  key: 'counter',
  default: {
    label: 'Count',
    count: 0,
    add: 0,
  },
  // dangerouslyAllowMutability: true,
});

function SettingsScreen() {
  const [count, setCount] = useRecoilState(counter);
  const resetCounter = useResetRecoilState(counter);
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>
        {count.label} : {count.count}
      </Text>
      <Text>{count.add}</Text>
      <Button
        title="increment"
        onPress={() => {
          let newCount = {...count};
          newCount.count = count.count + 1;
          setCount(newCount);
        }}
      />
      <Button
        title="reset"
        onPress={() => {
          resetCounter();
        }}
      />
    </View>
  );
}

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Reanimated"
      tabBar={(props) => <MyTabBar {...props} />}>
      <Tab.Screen name="Settings" component={ScrollEvent} />
      <Tab.Screen name="Gallery" component={Gallery} />
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Reanimated" component={Reanimate} />
      <Tab.Screen name="VerticalGallery" component={VerticalGallery} />
    </Tab.Navigator>
  );
}

let codePushOptions = {checkFrequency: codePush.CheckFrequency.MANUAL};

function App() {
  return (
    <RecoilRoot>
      <NotifyProvider>
        <NavigationContainer>
          <MyTabs />
        </NavigationContainer>
      </NotifyProvider>
    </RecoilRoot>
  );
}

export default codePush(codePushOptions)(App);
