/**
 * @format
 */

import {AppRegistry, LogBox} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {tachyonsConfig} from './theme/config';
import '@react-native-firebase/crashlytics';

// if (__DEV__) {
//   import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
// }

LogBox.ignoreAllLogs();

tachyonsConfig();

AppRegistry.registerComponent(appName, () => App);
